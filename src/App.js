import React from 'react';
import logo from './logo.svg';
import 'antd/dist/antd.css';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Login from'./Login/Login.js' ;
import Header from'./Index/Header.js' ;
import Content from'./Index/Content.js' ;
import { applyMiddleware, createStore } from 'redux';
import { Provider } from 'react-redux';
import appReducer from './reducer';
import logger from 'redux-logger'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { Container, Row, Col, Button } from 'reactstrap';
const store = createStore(
  appReducer,
  applyMiddleware(logger)
)


console.log('Default:', store.getState());

console.log('TOGGLE_STATUS', store.getState());

function AppRouter() {
  return (
      <div>
        <Provider store={store}>
          <Router>
            <Header></Header>
            <Content></Content>
          </Router>
        </Provider>
      </div>
    );
}

export default AppRouter;
