import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter as Router, Route, Link,BrowserRouter,withRouter , Switch } from "react-router-dom";
import {  Redirect } from 'react-router';
import { NavLink as RRNavLink } from 'react-router-dom';

import Login from'../Login/Login.js' ;
import Profile from'../Index/Profile.js' ;
import Home from'../Index/Home.js' ;
import Lixi from'../Index/Lixi.js' ;
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
} from 'reactstrap';
import { logoutUser } from '../actions/user.js';

const Button = () => (
    <Route render={({ history}) => (
      <button
        type='button'
        onClick={() => { history.push('/profile') }}
      >
        Click Me!
      </button>
    )} />
  )


function SetHeaderLogin(){
    return <Login></Login>
}
function SetProfile(){
    return <Profile></Profile>
}
function SetLixi(){
    return <Lixi></Lixi>
}
function SetHome(){
    return <Home></Home>
}
class Header extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            login: true,isOpen: false,log:JSON.parse(localStorage.getItem('user')),
        };
    }
    toggle=()=> {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    render(){
        function Index() {
            return <Home></Home>;
        }
      
        const Logout =()=>{
            localStorage.removeItem("user");
        }
        function Profile() {
            return <SetProfile></SetProfile>;
        }
        function Lixi() {
            return <SetLixi></SetLixi>;
        }
        function Logins() {
            return <SetHeaderLogin></SetHeaderLogin>;
        }   
        if(localStorage.getItem('user')){
            return (
                <BrowserRouter>
                <div>
                    <div>
                
                    <Navbar color="light" light expand="md">
                            <NavbarBrand tag={RRNavLink} exact to="/" activeClassName="active"><img src="https://pbs.twimg.com/profile_images/378800000404369978/89d4049297491dd4c063cf071da47404_400x400.png" style={{width: '30px', height: '30px'}} alt=""/></NavbarBrand>
                            <NavbarToggler onClick={this.toggle} />
                            <Collapse isOpen={this.state.isOpen} navbar>
                                <Nav className="ml-auto" navbar>
                                
                                    <NavItem>
                                        <NavLink tag={RRNavLink} exact to="/profile" activeClassName="active">Profile: {this.state.log.username}</NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink tag={RRNavLink} exact to="/lixi">Quay lì xì</NavLink>
                                    </NavItem>
                                    <UncontrolledDropdown nav inNavbar>
                                        <DropdownToggle nav caret>
                                            Options
                                        </DropdownToggle>
                                        <DropdownMenu right>
                                            <DropdownItem>
                                                Option 1
                                        </DropdownItem>
                                            <DropdownItem>
                                                Option 2
                                        </DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem >
                                                <NavLink tag={RRNavLink} exact to="/login" activeClassName="active" onClick={Logout}>Logout:</NavLink>
                                        </DropdownItem> 
                                        </DropdownMenu>
                                    </UncontrolledDropdown>
                                </Nav>
                            </Collapse>
                            
                    
                    </Navbar>
                    
                <Switch>
                <Route exact  path="/" exact component={Index} />
                <Route path="/login/" component={Logins} />
                <Route path="/profile/" component={Profile} />
                <Route path="/lixi/" component={Lixi} />
                </Switch>
                    </div>
                </div>
                </BrowserRouter>  
               );
        }else{
            return (
                <div>
                    <Switch>
                        <SetHeaderLogin></SetHeaderLogin>
                        <Route exact  path="/" exact component={Index} />
                        <Route path="/login" component={Logins} />
                        <Route path="/profile" component={Profile} />
                        <Route path="/lixi" component={Lixi} />
                    </Switch>
                </div>
               );
        }

     }
}

Header.propTypes = {

};

export default Header;