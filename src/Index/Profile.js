import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Container, Row, Col } from 'reactstrap';
import * as actions from '../actions/user.js'
import { Descriptions,Avatar } from 'antd';
import { connect } from "react-redux";
import Base64 from 'js-base64';
class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
          log:JSON.parse(localStorage.getItem('user')),
          messages: this.props.messages,  
      };

      
    }
  
    componentWillMount() {
      this.props.logoutUser();
    }

    componentDidMount() {

    }

    

    render() {     
        return (
          <div style={divStyle}>
         
          <Descriptions title="User Info">
            <Descriptions.Item label=""><Avatar size={120} src={this.state.log.avatar} ></Avatar></Descriptions.Item>
            <Descriptions.Item label="UserName">{this.state.log.username}</Descriptions.Item>
            <Descriptions.Item label="Telephone">1810000000</Descriptions.Item>
            <Descriptions.Item label="Live">Hangzhou, Zhejiang</Descriptions.Item>
            <Descriptions.Item label="Remark">empty</Descriptions.Item>
            <Descriptions.Item label="Address">
              No. 18, Wantang Road, Xihu District, Hangzhou, Zhejiang, China
            </Descriptions.Item>
          </Descriptions></div>
        );
    }
}

const divStyle = {
  marginLeft:"300px",
  marginTop:'100px'

} ;
const mapStateToProps = state => {
  console.log(state);
  return {
    messages: state.user.all_message,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchAllUser: (messages) => dispatch(actions.fetchAllUser(messages)),
    logoutUser: (messages) => dispatch(actions.logoutUser(messages)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Profile);