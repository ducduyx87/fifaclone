import React, { Component } from 'react';
import PropTypes from 'prop-types';
import data from '../data.json';
import StarRatings from 'react-star-ratings';
import { Container, Row, Col, Button } from 'reactstrap';
import _ from 'lodash';
import ReactStars from 'react-rating-stars-component'
const Notification = (props) => {
    const { nameFC, league ,img,stars} = props;
    return (
        <div>
            <img src={img} style={{width:'80%',marginLeft:'15px'}}></img>
            <div >
                <div style={{textAlign:'center'}}>
                <StarRatings
                    rating={stars}
                    starRatedColor="#FDBA43"
                    starDimension='12px'
                    starSpacing='2px' style={{marginLeft:'15px'}}/></div>
                <p style={{textAlign:'center'}}>{nameFC}</p>
                <p style={{textAlign:'center'}}>{league}</p>
                
            </div>
        </div>
    );
  };
class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rating:0,
            rating2:0,
            pickP1:'http://royalewellness.info/wp-content/uploads/2018/11/blank-family-crest-group-free-template-strand-is-also-called-shield-templates-design-soccer.jpg',
            pickP2:'http://royalewellness.info/wp-content/uploads/2018/11/blank-family-crest-group-free-template-strand-is-also-called-shield-templates-design-soccer.jpg',
            nameFC1:'No name',
            nameFC2:'No name',
            league1:'No league',
            league2:'No league',
            stars1:0,
            stars2:0
        };
    }
    changeRating = (newRating) => {
        this.setState({
          rating: newRating
        })
    }
      changeRating2 = (newRating) => {
        this.setState({
          rating2: newRating
        })
        
    }
    
    componentWillMount() {
    
    }

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {

    }

    shouldComponentUpdate(nextProps, nextState) {
        return true;
    }

    componentWillUpdate(nextProps, nextState) {
        console.log(this.state.rating);
        
    }

    componentDidUpdate(prevProps, prevState) {
        console.log(this.state.rating);
    }

    componentWillUnmount() {

    }
    _rollClub =() =>{
        var obj =  data.infoClub;

        if(this.state.rating!==this.state.rating2)
        var list = _.filter(obj, item =>  item.stars >= this.state.rating &&  item.stars <= this.state.rating2);
        else
        var list = _.filter(obj, item =>  item.stars=== this.state.rating );
        var response = JSON.stringify(list);
        let roll = JSON.parse(response);
        var rand=Math.floor(Math.random() *  list.length);
        if(roll.length!==0){
           
                this.setState({
                    pickP1: roll[rand]['img'],
                    nameFC1:roll[rand]['name'],
                    league1:roll[rand]['league'],
                    stars1:roll[rand]['stars'],

                })
        
            roll=JSON.parse(JSON.stringify(roll));
            
            var roll2 = _.filter(roll, item =>  item.name!== roll[rand]['name']);
            var rand2=Math.floor(Math.random() *  roll2.length);
          
                this.setState({
                    pickP2: roll2[rand2]['img'],
                    nameFC2:roll2[rand2]['name'],
                    league2:roll2[rand2]['league'],
                    stars2:roll2[rand2]['stars'],
                })
           
        }
        
    }
    render() {
        const ratingChanged = (newRating) => {
            this.setState({
                rating: newRating
              })
          }
          
        const ratingChanged2 = (newRating) => {
        this.setState({
            rating2: newRating
            })
        }
        return (
                   <Container>
                            <Row style={{marginTop:'50px'}}>
                                <Col sm='12' md="12" style={{marginLeft:'20%'}}>
                                    <Col md='12' ><a style={textMargin}>Từ : </a>
                                    <ReactStars
                                    count={5}
                                    onChange={ratingChanged}
                                    size={35}
                                    color2={'#ffd700'}
                                    value={this.state.rating} />
                                    </Col>
                                    <Col md='12' ><a style={textMargin}>Tới: </a>
                                    <ReactStars
                                    count={5}
                                    onChange={ratingChanged2}
                                    size={35}
                                    color2={'#ffd700'} 
                                    value={this.state.rating2}/>
                                    </Col>
                                </Col>
                                <Col sm='12' md="12" style={{marginLeft:'30%'}}>
                                    <Button color="warning" outline  style={{marginTop:'14px',marginLeft:'10px'}} size="lg" onClick={this._rollClub}>Quay</Button>
                                </Col>
                            </Row>
                           <Row style={{marginTop:'20%'}}>
                                <Col xs="5"><Notification nameFC={this.state.nameFC1} league={this.state.league1} img={this.state.pickP1} stars={this.state.stars1}></Notification></Col>
                                <Col xs="2"><h3 style={{marginTop:'70px'}}>VS</h3></Col>
                                <Col xs="5"><Notification nameFC={this.state.nameFC2} league={this.state.league2} img={this.state.pickP2} stars={this.state.stars2}></Notification></Col>
                            </Row>
                            
                    </Container>
          );
    }
}
const textMargin = {
    top: '15px', position: 'relative',float: 'left', right:'5px'
};
export default Home;