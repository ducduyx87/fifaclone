import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as actions from '../actions/user.js'
import { Descriptions,Avatar } from 'antd';
import { connect } from "react-redux";
import { Container, Row, Col, Button } from 'reactstrap';
import Base64 from 'js-base64';
const img1 ='https://vfirstads.com/wp-content/uploads/2018/07/Bao-lixi-VietBank-S1.jpg';
const img2 ='https://vfirstads.com/wp-content/uploads/2018/07/Bao-lixi-VietBank-S1.jpg';
const img3 ='https://vfirstads.com/wp-content/uploads/2018/07/Bao-lixi-VietBank-S1.jpg';
const cauchuc1="An khang thịnh vượng nhé";
const cauchuc2="Ngày càng giàu nè";
const cauchuc3="Tuổi mưới hạnh phúc nè";
const cauchuc4="Ăn no chóng lớn nè";
const cauchuc5="Tiền vào như nước nè";
var data=[
    {img: img1,money:10000,cauchuc:cauchuc5},{img: img1,money:5000,cauchuc:cauchuc1},{img: img2,money:20000,cauchuc:cauchuc2}, {img: img1,money:10000,cauchuc:cauchuc4},{img: img2,money:20000,cauchuc:cauchuc1},{img: img3,money:50000,cauchuc:cauchuc3},{img: img3,money:30000,cauchuc:cauchuc3}
        ]
const Notification = (props) => {
    const { money ,img, cauchuc} = props;
    return (
        <div>
            <Row >
                <Col xs="12"><img src={img} style={{width:'200px',marginLeft:'70px'}}></img></Col>
                <Col xs="12"><p style={{textAlign:'center',marginTop:50,fontFamily :"Comic Sans MS",fontSize:30}}>{money} VNĐ</p></Col>
                <Col xs="12"><p style={{textAlign:'center',marginTop:10,fontFamily :"Comic Sans MS",fontSize:20}}>{cauchuc}</p></Col>
            </Row>
        </div>
    );
  };
class Lixi extends Component {
    constructor(props) {
        super(props);
        this.state = {
            money:0,
            cauchuc:'He he he cố lên nào',
            img:img1
      };

      
    }
  
    componentWillMount() {
      this.props.logoutUser();
    }

    componentDidMount() {

    }

    _rollClub =() =>{
        var obj =  data;
        var rand=Math.floor(Math.random() * obj.length);
        if(obj.length!==0){
            this.setState({
                money: Number(obj[rand]['money']).toLocaleString('en'),
                cauchuc:obj[rand]['cauchuc'],
                img:obj[rand]['img']
            })
        }
        console.log(this.state);
        
    }

    render() {     
        return (
          <div >
          <Container >
                <Row style={{marginTop:'50px',fontFamily :"Comic Sans MS"}}>
                    <Col sm='12' md="12">
                        <Button color="warning" outline  style={{marginTop:'14px',marginLeft:'130px'}} size="lg" onClick={this._rollClub}>Bốc</Button>
                    </Col>
                </Row>
                <Row style={{marginTop:'20%'}}>
                    <Col xs="12"><Notification cauchuc={this.state.cauchuc} img={this.state.img} money={this.state.money} ></Notification></Col>
                </Row>
                
        </Container>    
        </div>
        );
    }
}

// const divStyle = {
//   marginLeft:"300px",
//   marginTop:'100px'

// } ;
const mapStateToProps = state => {
  console.log(state);
  return {
    messages: state.user.all_message,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchAllUser: (messages) => dispatch(actions.fetchAllUser(messages)),
    logoutUser: (messages) => dispatch(actions.logoutUser(messages)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Lixi);