const initialState = {
	// role: 'user',
}
const myUser = (state = initialState, action) => {
   
    switch(action.type){
        case 'logout':
            return state;
        case 'login':
            return Object.assign({},state,{
                username:action.username,
                uid:action.uid,
                messages:action.messages
            });
        case 'ALL_USER':
            return Object.assign({},state,{
                messages:action.messages
            })
        default:
            return state;
    }

}
export default myUser;