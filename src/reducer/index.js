import { combineReducers } from 'redux';
import myUser from './user';
import myReducer from './test';

const appReducer = combineReducers({
    // session: configureSession,
    user: myUser,
    test:myReducer
  });
  
  export default appReducer;
  