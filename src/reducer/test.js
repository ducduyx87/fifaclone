const initialState = {
    status : false
  }
  
const myReducer = (state = initialState, action) => {
    if (action.type === 'TOGGLE_STATUS') {
        let newState = {...state}
        newState.status = !state.status;
        return newState;// muc dich cua reducer la tra ra cai state moi
    }
    
    return state;
}
  export default myReducer;