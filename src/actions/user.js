export const fetchAllUser=(mess)=>{
    return {
        type:'ALL_USER',
        messages:mess
    }
}
export const logoutUser=(mess)=>{
    return {
        type:'logout',
        messages:mess
    }
}
export const loginUser=(uid,username,mess)=>{
    return {
        type:'login',
        uid:uid,
        username:username,
        messages:mess
    }
}
export const createNewUser=(mess)=>{
    return{
        type:'CREATE_USER',
        messages:mess
    }
}