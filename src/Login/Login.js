import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Home from'../Index/Home.js' ;
import '../App.css';
import axios from 'axios';
import Message from 'antd/lib/message';
import * as actions from '../actions/user';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import { BrowserRouter as Router, Route, Redirect,withRouter } from 'react-router-dom';
import { connect } from "react-redux";
import { Container, Row, Col } from 'reactstrap';
import isEmpty from 'lodash/isEmpty';
import {Base64} from 'js-base64';
const _ = { isEmpty }
class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            password: '',
            account: '',
            login:false,
            persons: []
        };
       
    }
    componentDidMount() {
        
        axios.get(`https://5ce4f749c1ee360014725f29.mockapi.io/tbl_login`)
          .then(res => {
            const persons = res.data;
            this.setState({ persons });
        });
        
        
        
    }
    updateInputValue=(e)=>{
        this.setState({
            account: e.target.value
          });
     
    }
    updateInputValue2=(e)=>{
        this.setState({
            password: e.target.value
          });
          console.log( e.target.value);
    }
    updateInputLogin=(e)=>{
        this.setState({
            password:e
          });
    }
    handleSubmit=()=>{
        if(_.isEmpty(this.state.account)){
        Message.error('Bạn chưa nhập acc');
        return;
        };
        if(_.isEmpty(this.state.password)){
        Message.error('Bạn chưa nhập mật khẩu');
        return;
        };
        var{account,password,persons2}=this.state;
        var i = 0;
        this.state.persons.map(function(persons){
            if(account===persons.account && password===persons.pass){
            //    var password=Base64.encode(account);
                i++;
                localStorage.setItem('user',JSON.stringify({
                    username:account,
                    // password:password,
                    id:persons.id,
                    avatar:persons.avatar,
                    token:persons.token,
                }));
            }else{
                if(i === 0){
                    Message.error('Mật khẩu của bạn chưa chính xác');
                    return;
                }
            }
        });
        var log=localStorage.getItem('user');
        if(log){
            this.setState({
                login:true
              });
              this.handlingButtonClick();
            return 
        }
   
    }
    handlingButtonClick = () => {
        this.props.history.push("/") //doing redirect here.
    }
    
    render() {
        
        if(!localStorage.getItem('user')){
            return (
                <Container>
                    <Row>
                <div style={boxStyle}>
                    <Form className="login-form">
                        <Form.Item>
                        
                            <Input
                            prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            placeholder="Username" value={this.state.account} onChange={e => this.updateInputValue(e)}
                            />
                        
                        </Form.Item>
                        <Form.Item>
                        
                            <Input type="password" placeholder="Password"
                            prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            placeholder="test" value={this.state.password} onChange={e => this.updateInputValue2(e)}
                            />
                        
                        </Form.Item>
                    
                        <Form.Item>
                        <Checkbox>Remember me</Checkbox>
                    
                        <Button className="login-form-button" onClick={(e)=>this.handleSubmit(e)}>
                            Log in
                        </Button>
                        Or <a href="">register now!</a>
                        </Form.Item>
                    </Form>
                    {/* <ul>
                        { this.state.persons.map(person => <li><img src={person.avatar} /></li>)}
                    </ul> */}
                </div>
                    </Row>
                </Container>
            );
        }else{
            return <Redirect to='/'/>
        }
    }
}
const boxStyle = {
    fontSize: '15px',
    textAlign: 'center',
    width: '300px',
    marginLeft:'30px',
    marginTop:'200px'
  };
  
  const mapStateToProps = (state) => {
    return {
        token: state.user.token,
        messages: state.user.all_message,
    };
  };
  
  const mapDispatchToProps = (dispatch) => {
    return {
      fetchAllUser: (messages) => dispatch(actions.fetchAllUser(messages)),
      logoutUser: (messages) => dispatch(actions.logoutUser(messages)),
      loginUser: (messages) => dispatch(actions.logoutUser(messages)),

    };
  };
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login));